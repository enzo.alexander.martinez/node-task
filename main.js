const express = require("express");
const app = express();
const PORT = 3000;
// build small REST API with Express

console.log("Server-side program starting...")

// baseurl: https://localhost:3000
// Endpoint: https://localhost:3000/
app.get('/', (req, res) => {
    res.send('Hello world');
})

/**
 * This is an arrow function that adds two numbers together.
 * @param {Number} a first param
 * @param {Number} b second param
 * @returns {Number}
 */
const add = (a, b) => {
    const sum = a + b;
    return sum;
};

// Adding endpoint: https://localhost:3000/add?a=value&b=value
app.get('/add', (req, res) => {
    console.log(req.query);
    const sum = add(Number(req.query.a), Number(req.query.b));
    res.send(sum.toString());
});

app.listen(PORT, () => console.log(
    `server listening https://localhost:${PORT}`
));